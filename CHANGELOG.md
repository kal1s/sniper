## Sniper v0.4.3
### Improvement
- ScanPort: created a protocol selector to select tcp or udp

<br/>

## Sniper v0.4.2
### Bugfix
- ScanPort: added chunk of ports to do the scan to not be ofuscated with the open client ports limits 

<br/>

## Sniper v0.4.1
### Improvement
- DoSPort: created a protocol selector to select tcp or udp

### Bugfix
- ScanPort: correction on timeout because didn't working correctly 

<br/>

## Sniper v0.4.0
### Feature
- DoSHTTP Module created `sniper doshttp`.
- Licensed with GNU GPLv3
- Created new repository to open for public `gitlab.com/kal1s/spider`

<br/>

## Sniper v0.3.0
### Feature

- DoSPort Module created `sniper dosport`.

<br/>

## Sniper v0.2.0
### Feature

- ScanPort Module created `sniper scanport`.

<br/>

## Sniper v0.1.0
### Feature

- DNS Name Resolution Module created `sniper dns`.