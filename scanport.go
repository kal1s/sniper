// Package scanport from sniper module that implements everything about Port Scanning

package sniper

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"time"
)

// Global Variable to storage the Open Ports to return to the Caller as a Slice of int
var openports = make([]int, 0)

// ScanPort :
// Call ScanPort to scan a range of ports
func ScanPort(host string, protocol string, firstPort int, lastPort int, timeout int) (openPorts []int) {

	goend := make(chan bool)

	counter := (lastPort - firstPort) + 2

	for port := firstPort; port <= lastPort; port++ {

		if counter == 60000 ||
			counter == 55000 ||
			counter == 50000 ||
			counter == 45000 ||
			counter == 40000 ||
			counter == 35000 ||
			counter == 30000 ||
			counter == 25000 ||
			counter == 20000 ||
			counter == 15000 ||
			counter == 10000 ||
			counter == 5000 {

			time.Sleep(time.Duration(timeout) * time.Second)
		}

		go scanPort(host, protocol, port, timeout, goend)

		counter--
	}

	time.Sleep(time.Duration(timeout) * time.Second)

	return openports
}

// scanPort is Called by ScanPort and used to scan each port using Go Routines
func scanPort(host string, protocol string, port int, timeout int, goend chan bool) {

	target := fmt.Sprintf("%s:%d", host, port)

	switch protocol {
	case "tcp":
		conn, err := net.Dial("tcp", target)

		if err != nil {
			return
		}

		openports = append(openports, port)

		conn.Close()

		goend <- true

	case "udp":

		p := make([]byte, 2048)

		conn, err := net.DialTimeout("udp", target, time.Duration(2)*time.Second)

		if err != nil {
			return
		}

		fmt.Fprintf(conn, "request Sniper/7")

		_, err = bufio.NewReader(conn).Read(p)

		if err != nil {
			return
		}

		openports = append(openports, port)

		conn.Close()

		goend <- true

	default:
		fmt.Println("[-] Invalid Protocol")
		fmt.Println("")
		os.Exit(1)
	}

}
