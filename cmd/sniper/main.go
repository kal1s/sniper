package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/kal1s/sniper"
)

func main() {

	// Printing in the screen the Sniper Version
	fmt.Println(sniper.Version())

	// Printing in the screen the Sniper Help
	if len(os.Args) < 2 {
		fmt.Println(sniper.Help())
		return
	}

	// Rizing the Linux and macOS Open File Limits
	// Doesn't work for Windows, because that is necessary to comment that lines to compile for Windows
	// var limit syscall.Rlimit
	// limit.Max = 65535
	// limit.Cur = 65535
	// syscall.Setrlimit(syscall.RLIMIT_NOFILE, &limit)

	// Setting the dns command and arguments
	dnsCMD := flag.NewFlagSet("dns", flag.ExitOnError)
	dnsHost := dnsCMD.String("host", "localhost", "Host IP or DNS Name")

	// Setting the DoSHTTP command and arguments
	doshttpCMD := flag.NewFlagSet("doshttp", flag.ExitOnError)
	doshttpURL := doshttpCMD.String("url", "http://localhost:80", "Complete URL")
	doshttpAgent := doshttpCMD.String("agent", "Sniper/7", "HTTP Agent")
	doshttpReq := doshttpCMD.Int("requests", 10, "Amount of Requests")
	doshttpTimeout := doshttpCMD.Int("timeout", 30, "Timeout between attacks")

	// Setting the DoSPort command and arguments
	dosportCMD := flag.NewFlagSet("dosport", flag.ExitOnError)
	dosportHost := dosportCMD.String("host", "localhost", "Host IP or DNS Name")
	dosportPort := dosportCMD.Int("port", 80, "Port Number")
	dosportProtocol := dosportCMD.String("protocol", "tcp", "Protocol Selector (tcp or udp)")
	dosportPkts := dosportCMD.Int("packets", 10, "Amount of Packets")
	dosportTimeout := dosportCMD.Int("timeout", 30, "Timeout between attacks")

	// Setting the ScanPort command and arguments
	scanportCMD := flag.NewFlagSet("scanport", flag.ExitOnError)
	scanportHost := scanportCMD.String("host", "localhost", "Host IP or DNS Name")
	scanportProtocol := scanportCMD.String("protocol", "tcp", "Protocol Selector (tcp or udp)")
	scanportFrom := scanportCMD.Int("from", 1, "Initial Port Number")
	scanportTo := scanportCMD.Int("to", 65535, "Final Port Number")
	scanportTimeout := scanportCMD.Int("timeout", 5, "Timeout between scans")

	// Selecting the command and arguments
	switch os.Args[1] {

	// Selecting dns and their arguments
	case "dns":
		dnsCMD.Parse(os.Args[2:])
		dns(*dnsHost)

	// Selecting dosHTTP and their arguments
	case "doshttp":
		doshttpCMD.Parse(os.Args[2:])
		doshttp(*doshttpURL, *doshttpAgent, *doshttpReq, *doshttpTimeout)

	// Selecting DoSPort and their arguments
	case "dosport":
		dosportCMD.Parse(os.Args[2:])
		dosport(*dosportHost, *dosportPort, *dosportPkts, *dosportProtocol, *dosportTimeout)

	// Selecting ScanPort and their arguments
	case "scanport":
		scanportCMD.Parse(os.Args[2:])
		scanport(*scanportHost, *scanportProtocol, *scanportFrom, *scanportTo, *scanportTimeout)

	// Printing Help in case no command was selected
	default:
		fmt.Println(sniper.Help())
	}
}
