package main

import (
	"fmt"

	"gitlab.com/kal1s/sniper"
)

// Function used in the main package to Call sniper.DoSHTTP Module
func doshttp(url, agent string, requests, timeout int) {

	// Printing in the screen some informations about the attack
	fmt.Println("")
	fmt.Println("[ DoSHTTP ] -> Running HTTP DoS")
	fmt.Println("")
	fmt.Println("[+] URL:     ", url)
	fmt.Println("[+] Requests:", requests)
	fmt.Println("[+] Timeout: ", timeout)

	fmt.Println("")
	fmt.Println("[ Press CTRL+C to Stop ]")
	fmt.Println("")

	// Calling the Function to execute the attack
	sniper.DoSHTTP(url, agent, requests, timeout)

	// Printing Blank Line to have more space in the screen
	fmt.Println("")
}
