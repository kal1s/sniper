package main

import (
	"fmt"

	"gitlab.com/kal1s/sniper"
)

// Function used in the main package to Call sniper.ScanPort Module
func scanport(host, protocol string, from, to, timeout int) {

	// Printing in the screen a Header to print the results
	fmt.Println("")
	fmt.Println("[ ScanPort ] ->", host, "|", protocol)
	fmt.Println("")

	// Calling the Function and storing the results
	openPorts := sniper.ScanPort(host, protocol, from, to, timeout)

	// Printing in the screen the results
	for _, port := range openPorts {
		fmt.Println("[+]", port)
	}

	// Printing Blank Line to have more space in the screen
	fmt.Println("")
}
