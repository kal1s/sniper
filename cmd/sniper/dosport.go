package main

import (
	"fmt"

	"gitlab.com/kal1s/sniper"
)

// Function used in the main package to Call sniper.DoSPort Module
func dosport(host string, port int, packets int, protocol string, timeout int) {

	// Printing in the screen some informations about the attack
	fmt.Println("")
	fmt.Println("[ DoSPort ] -> Running Port DoS")
	fmt.Println("")
	fmt.Println("[+] Host:    ", host)
	fmt.Println("[+] Port:    ", port)
	fmt.Println("[+] Packets: ", packets)
	fmt.Println("[+] Protocol:", protocol)
	fmt.Println("[+] Timeout: ", timeout)

	fmt.Println("")
	fmt.Println("[ Press CTRL+C to Stop ]")
	fmt.Println("")

	// Calling the Function to execute the attack
	sniper.DoSPort(host, port, packets, protocol, timeout)

	// Printing Blank Line to have more space in the screen
	fmt.Println("")
}
