package main

import (
	"fmt"

	"gitlab.com/kal1s/sniper"
)

// Function used in the main package to Call sniper.DNSResolver Module
func dns(host string) {

	// Calling the Function and storing the results
	resp, err := sniper.DNSResolver(host)

	// Abort if something wrong happened
	if err != nil {
		fmt.Println("")
		fmt.Println("[ DNS ] ->", host)
		fmt.Println(" ")
		fmt.Println("[-] Was not possible to resolve this Host")
		fmt.Println("")
		return
	}

	// Printing in the screen a Header to print the results
	fmt.Println("")
	fmt.Println("[ DNS ] ->", host)
	fmt.Println("")

	// Printing in the screen the results
	for _, host := range resp {
		fmt.Println("[+]", host)
	}

	// Printing Blank Line to have more space in the screen
	fmt.Println("")
}
