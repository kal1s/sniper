// Package dns from sniper module that implements everything about DNS Resolver

package sniper

import (
	"net"
)

// DNSResolver :
// Call DNSNameResolver to resolve DNS Name to IP Address and returns a slice of strings with results and errors
// Call DNSIPResolver to resolve IP Address to DNS Name and returns a slice of strings with results and errors
func DNSResolver(host string) (result []string, err error) {

	if net.ParseIP(host) != nil {
		return dnsIPResolver(host)
	}

	return dnsNameResolver(host)
}

// dnsNameResolver :
// Resolves DNS Names to IP Address and returns a slice of strings with results and errors
func dnsNameResolver(name string) (result []string, err error) {

	return net.LookupHost(name)

}

// dnsIPResolver :
// Resolves IP Address to DNS Names and returns a slice of strings with results and errors
func dnsIPResolver(ip string) (result []string, err error) {

	return net.LookupAddr(ip)

}
