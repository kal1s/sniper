// Package help from sniper module that implements everything about Version and Help

package sniper

// Version :
// Returns the Version and Message
func Version() string {

	return `
     _________       __                     
    /   _____/ ____ |__|______  ____ _____  
    \_____  \ /    \|  |   _  \/ __ \  __ \
     /       \   |  \  |  |_|    ___/  | \/
    /______  /___|  /__|   ___/\__  |__|   
           \/     \/   |__|       \/         
              by kal1s | v0.4.3
`
}

// Help :
// Returns the Sniper Help Text Terminal
func Help() string {
	return `
Usage:
	sniper <command> [-arguments]


dns
	-host		Host IP or DNS Name

doshttp
	-url		Complete URL
	-agent		HTTP Agent
	-requests	Amount of Requests
	-timeout	Timeout between attacks

dosport
	-host		Host IP or DNS Name
	-port		Port Number
	-protocol  	Protocol Selector (tcp or udp)
	-packets	Amount of Packets
	-timeout	Timeout between attacks
	
scanport
	-host		Host IP or DNS Name
	-protocol  	Protocol Selector (tcp or udp)
	-from		Initial Port Number
	-to		Final Port Number
	-timeout	Timeout between scans
	`
}
